var mongoose = require('mongoose');

var ReviewSchema = mongoose.Schema({
    rating:{type:Number,default:null},
    content:{type:String,default:null}
});
var proudctSchema=mongoose.Schema({
    productName:{type:String},
    basePrice:{type:String},
    category:{type:String},
    brand:{type:String},
    productMaterial:{type:String},
    imageUrl:{type:String},
    delivery:{type:String},
    details:{type:String},
    reviews :[ReviewSchema]
});
module.exports = mongoose.model('products',proudctSchema) ;