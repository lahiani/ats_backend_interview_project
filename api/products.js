var products = require('../models/productSchema');
var request = require("request");
var express = require('express');
var router = express.Router();
module.exports = router;
//import all the products
router.get('/', function (req, res, next) {
    products.find(function (err, product) {
        if (err) {
            res.send(err)
            console.log('err')
        }
        if (!product) {
            res.status(404).send();
            console.log('product')
        }
        else {
            res.json(product);
            console.log('json')
        }

    })
});
//import all the categorys
router.get('/cat', function (req, res, next) {
    products.collection.distinct("category", function (err, product) {
        if (err) {
            res.send(err);
            console.log('err')
        }
        if (!product) {
            res.status(404).send();
            console.log('product')
        }
        else {
            res.json(product);
            console.log('json')
        }

    })
});
//import all the data into mongodb
router.get('/import', function (req, res, next) {
    var jsonArr = [];
    var url = "http://internal.ats-digital.com:3066/api/products";
    request({
        url: url,
        json: true
    }, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            for (i in body) {
                var data = {
                    "productName": body[i].productName,
                    "basePrice": body[i].basePrice,
                    "category": body[i].category,
                    "brand": body[i].brand,
                    "productMaterial": body[i].productMaterial,
                    "imageUrl": body[i].imageUrl,
                    "delivery": body[i].delivery,
                    "details": body[i].details,
                };
                var prod = new products(data);
                for (j in body[i].reviews) {
                    var rev = {
                        "rating": body[i].reviews[j].rating,
                        "content": body[i].reviews[j].content
                    }
                    prod.reviews.push(rev);
                }
                prod.save(function (err, prod) {
                    if (err)
                        res.send(err);
                });
            }
        }
    })
});
module.exports = router;